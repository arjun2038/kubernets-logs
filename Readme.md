## Collecting kubernetes logs using Node Level Logging
- /var/log/pods/: Under this location, the container logs are organized in separate pod folders. /var/log/pods/<namespace>_<pod_name>_<pod_id>/<container_name>/. Each pod folder contains the individual container folder and its respective log file.
- Collecting log from this location using fluentd
- Using Daemonset, creating fluentd log collector for each node
